package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import frameworkbase.ProjectWrapper;
import pages.Home;

public class SampleTest extends ProjectWrapper{

	@BeforeClass
	public void steData() {

		testCaseName ="sample";
		testCaseDescription = "sample test";
		author = "Sruthi";
		category = "sanity";
		wbookName = "testbook";
	} 

	@Test(dataProvider="fetchData")
	public void registration(String fname, String lname) throws InterruptedException {

		new Home().moveToSearchJob()
		.clickSiAdvantage()
		.clickRegister()
		.enterFirstName(fname)
		.enterLastName(lname)
		.selectProvince()
		.verifyPhoneNumber()
		.enterPassword()
		.selectResident()
		.selectCompanyType()
		.clickNext();

	}

}
