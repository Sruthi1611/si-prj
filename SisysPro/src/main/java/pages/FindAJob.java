package pages;

import org.openqa.selenium.WebElement;

import frameworkbase.BaseMethod;

public class FindAJob extends BaseMethod {

	public Registration clickRegister() throws InterruptedException {
		
		Thread.sleep(3000);
		WebElement register = locateElement("xpath","(//a[text()='Register'])[2]");
		click(register);
		return new Registration();
	}
}
