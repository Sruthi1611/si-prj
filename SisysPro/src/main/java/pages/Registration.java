package pages;

import java.util.List;
import org.openqa.selenium.WebElement;
import frameworkbase.BaseMethod;

public class Registration extends BaseMethod {

	public Registration enterFirstName(String name) {

		WebElement frstnm = locateElement("id", "firstname");
		type(frstnm, name);
		return this;
	}
	public Registration enterLastName(String lastname) {
		WebElement lstnm = locateElement("id", "lastname");
		type(lstnm, lastname);
		return this;
	}

	public Registration selectProvince() {

		WebElement prvn = locateElement("id", "Province");
		selectDropDownUsingText(prvn, "Ontario");
		return this; 
	}
	public Registration verifyPhoneNumber() {
		WebElement phnnum = locateElement("id", "mobphone");
		verifySelected(phnnum);
		return this;
	}

	public Registration enterPassword() {

		WebElement pwrd1 = locateElement("id", "pass1");
		WebElement pwrd2 = locateElement("id", "pass2");
		type(pwrd1, "asdf");
		type(pwrd2, "asdf");
		return this;

	}

	public Registration selectResident() {

		WebElement resid = locateElement("id", "canadaresidentyes");
		click(resid);
		return this;
	}

	public Registration selectCompanyType() {

		WebElement cmpnynm = locateElement("id", "incorpno");
		click(cmpnynm);
		return this;
	}

	public void clickNext() {

		WebElement nextbtn = locateElement("id", "Next");
		click(nextbtn);

		List<WebElement> errors = locateElements("xpath", "//span[@class='error']");
		if(errors == null) {
			movetonextStep();
		} else {
			for (WebElement error : errors) {
				System.out.println(error.getText());
			}
		}

	}
	public RegistrationSkills movetonextStep() {
		return new RegistrationSkills();
	}
}
