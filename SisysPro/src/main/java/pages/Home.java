package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import frameworkbase.BaseMethod;

public class Home extends BaseMethod {

	public Home moveToSearchJob() {
		
		Actions build = new Actions(driver);
		WebElement srchjb = locateElement("linktext", "Search Jobs");
		build.moveToElement(srchjb).perform();
		return this;
		
	}
	
	public FindAJob clickSiAdvantage() {
		WebElement siadv = locateElement("linktext", "S.i. Advantages");
		click(siadv);
		return new FindAJob();
	}
	
}
